package com.qatestlab.task4.tests;

import com.qatestlab.task4.base.BaseTest;
import com.qatestlab.task4.model.ProductData;
import com.qatestlab.task4.pages.DashboardPage;
import com.qatestlab.task4.pages.LoginPage;
import com.qatestlab.task4.pages.MainPage;
import com.qatestlab.task4.pages.ProductsPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    private LoginPage loginPage;
    private DashboardPage dashboardPage;
    private ProductsPage productsPage;
    private MainPage mainPage;
    private ProductData product;

    @Test
    public void launch() {
        loginPage = LoginPage.init(driver)
                .launch();
        Assert.assertTrue(loginPage.isAt());
    }

    @Test(dependsOnMethods = "launch", dataProvider = "Authentication")
    public void enterUserCredentialsAndSubmit(String username, String password) {
        loginPage.setUsername(username).
                setPassword(password).
                login();
        dashboardPage = DashboardPage.init(driver);
        Assert.assertTrue(dashboardPage.isAt());
    }

    @Test(dependsOnMethods = "enterUserCredentialsAndSubmit")
    public void goToCatalogOfProducts() {
        dashboardPage.navigateToCatalogOfProducts();
        productsPage = ProductsPage.init(driver);
        Assert.assertTrue(productsPage.isAt());
    }

    @Test(dependsOnMethods = "goToCatalogOfProducts")
    public void createNewProduct() {
        product = productsPage.addRandomProduct();
        Assert.assertNotNull(product);
    }

    @Test(dependsOnMethods = "createNewProduct")
    public void checkNewProduct() {
        mainPage = MainPage.init(driver)
                .launch();
        Assert.assertTrue(mainPage.isAt());
        mainPage.viewAllProducts();
        WebElement productLink = mainPage.findProductByName(product.getName());
        Assert.assertTrue(productLink.isDisplayed());
        productLink.click();
        ProductData parsedProduct = mainPage.parseData();
        Assert.assertEquals(product, parsedProduct);
    }
}