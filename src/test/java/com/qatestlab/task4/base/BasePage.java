package com.qatestlab.task4.base;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public abstract class BasePage {

    protected final WebDriver driver;

    protected BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public abstract boolean isAt();

    public boolean isAt(long timeout, TimeUnit timeunit) {
        try {
            await().atMost(timeout, timeunit)
                    .ignoreExceptions()
                    .until(this::isAt);
            return true;
        } catch(Exception e) {
            return false;
            }
    }

    protected WebElement getElementFluent(final By by) {
        return getElementFluent(by, 10, 2);
    }

    private WebElement getElementFluent(final By by, int timeout, int polling) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofSeconds(polling))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        WebElement element = wait.until(driver -> driver.findElement(by));
        return element;
    }
}