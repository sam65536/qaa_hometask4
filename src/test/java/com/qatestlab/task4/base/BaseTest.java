package com.qatestlab.task4.base;

import com.qatestlab.task4.utils.logging.EventHandler;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import static com.qatestlab.task4.utils.driver.DriverFactory.getDriver;
import static com.qatestlab.task4.utils.driver.DriverType.valueOf;
import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class BaseTest {

    protected EventFiringWebDriver driver;

    @DataProvider(name = "Authentication")
    public Object[][] credentials() {
        return new Object[][]{
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"},
        };
    }

    @BeforeClass
    @Parameters("browser")
    public void setUp(String browser) {
        driver = new EventFiringWebDriver(getDriver(valueOf(browser)));
        driver.register(new EventHandler());
        driver.manage().timeouts().implicitlyWait(15, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, SECONDS);
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}