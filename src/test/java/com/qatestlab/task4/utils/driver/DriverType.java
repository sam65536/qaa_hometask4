package com.qatestlab.task4.utils.driver;

public enum DriverType {
    CHROME,
    FIREFOX,
    IE
}
