package com.qatestlab.task4.utils.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static org.openqa.selenium.UnexpectedAlertBehaviour.ACCEPT;

public class DriverFactory {

    private static final ClassLoader classLoader = DriverFactory.class.getClassLoader();
    private static final Map<DriverType, Supplier<WebDriver>> driverMap = new HashMap<>();


    private static final Supplier<WebDriver> chromeDriverSupplier = () -> {
        System.setProperty("webdriver.chrome.driver",
                classLoader.getResource("chromedriver.exe").getPath());
        return new ChromeDriver();
    };

    private static final Supplier<WebDriver> firefoxDriverSupplier = () -> {
        System.setProperty("webdriver.gecko.driver",
                classLoader.getResource("geckodriver.exe").getPath());
        return new FirefoxDriver();
    };

    private static final Supplier<WebDriver> ieDriverSupplier = () -> {
        System.setProperty("webdriver.ie.driver",
                classLoader.getResource("IEDriverServer.exe").getPath());
        InternetExplorerOptions options = new InternetExplorerOptions()
                .destructivelyEnsureCleanSession()
                .requireWindowFocus()
                .enablePersistentHovering()
                .introduceFlakinessByIgnoringSecurityDomains()
                .setUnhandledPromptBehaviour(ACCEPT);
        return new InternetExplorerDriver(options);
    };

    static {
        driverMap.put(DriverType.CHROME, chromeDriverSupplier);
        driverMap.put(DriverType.FIREFOX, firefoxDriverSupplier);
        driverMap.put(DriverType.IE, ieDriverSupplier);
    }

    public static final WebDriver getDriver(DriverType type){
        return driverMap.get(type).get();
    }
}