package com.qatestlab.task4.pages;

import com.qatestlab.task4.base.BasePage;
import com.qatestlab.task4.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Optional;

public class ProductsPage extends BasePage {

    @CacheLookup
    @FindBy(how = How.CSS, using = "#page-header-desc-configuration-add")
    private WebElement addProductButton;

    private ProductsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public static ProductsPage init (WebDriver driver) {
        return new ProductsPage(driver);
    }

    @Override
    public boolean isAt() {
        Optional<Boolean> result = Optional.ofNullable(addProductButton.isDisplayed());
        return result != null ? true : false;
    }

    public ProductData addRandomProduct() {
        ProductData product = ProductData.generate();
        addProductButton.click();
        getElementFluent(By.id("form_step1_name_1")).clear();
        getElementFluent(By.id("form_step1_name_1")).sendKeys(product.getName());
        driver.findElement(By.id("form_step1_qty_0_shortcut")).clear();
        driver.findElement(By.id("form_step1_qty_0_shortcut")).sendKeys(product.getQty() + "");
        driver.findElement(By.id("form_step1_price_shortcut")).clear();
        driver.findElement(By.id("form_step1_price_shortcut")).sendKeys(product.getPrice());
        driver.findElement(By.xpath("//form[@id='form']/div[4]/div/div")).click();
        getElementFluent(By.cssSelector("div.growl-message"));
        getElementFluent(By.cssSelector("div.growl-close")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        getElementFluent(By.cssSelector("div.growl-message"));
        getElementFluent(By.cssSelector("div.growl-close")).click();
        return product;
    }
}