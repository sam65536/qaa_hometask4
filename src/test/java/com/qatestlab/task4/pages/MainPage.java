package com.qatestlab.task4.pages;

import com.qatestlab.task4.base.BasePage;
import com.qatestlab.task4.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Optional;

import static com.qatestlab.task4.utils.Properties.getBaseUrl;

public class MainPage extends BasePage {

    @FindBy(how = How.XPATH, using = "//section[@id='content']/section/a")
    private WebElement allProductsLink;

    private MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public static MainPage init (WebDriver driver) {
        return new MainPage(driver);
    }

    public MainPage launch() {
        driver.get(getBaseUrl());
        return this;
    }

    @Override
    public boolean isAt() {
        Optional<Boolean> result = Optional.ofNullable(allProductsLink.isDisplayed());
        return result != null ? true : false;
    }

    public void viewAllProducts() {
        allProductsLink.click();
    }

    public WebElement findProductByName(String name) {
        return getElementFluent(By.linkText(name));
    }

    public ProductData parseData() {
        String name = driver.findElement(By.xpath(".//*[@id='main']/div[1]/div[2]/h1")).getText();
        WebElement qtyContent = driver.findElement(By.cssSelector(".product-quantities>span"));
        int qty = Integer.parseInt(qtyContent.getText().split(" ")[0]);
        WebElement priceContent = driver.findElement(By.cssSelector(".current-price>span"));
        float price = Float.parseFloat(priceContent.getAttribute("content").split(" ")[0]);
        return new ProductData(name, qty, price);
    }
}
