package com.qatestlab.task4.pages;

import com.google.common.collect.ImmutableList;
import com.qatestlab.task4.base.BasePage;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.Optional;
import java.util.function.Consumer;

import static java.time.Duration.ofSeconds;

public class DashboardPage extends BasePage {

    @CacheLookup
    @FindBy(how = How.ID, using = "subtab-AdminCatalog")
    private WebElement catalogLink;

    @CacheLookup
    @FindBy(how = How.ID, using = "subtab-AdminProducts")
    private WebElement productsLink;

    @FindBy(how = How.CSS, using = ".img-thumbnail")
    private WebElement avatar;

    private DashboardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public static DashboardPage init (WebDriver driver) {
        return new DashboardPage(driver);
    }

    @Override
    public boolean isAt() {
        Optional<Boolean> result = Optional.ofNullable(avatar.isDisplayed());
        return result != null;
    }
    
    public void navigateToCatalogOfProducts() {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(ofSeconds(10))
                .pollingEvery(ofSeconds(1))
                .ignoreAll(ImmutableList.of(NoSuchElementException.class,
                        ElementNotVisibleException.class, StaleElementReferenceException.class));

        Actions action = new Actions(driver);
        Consumer<WebElement> hover = (WebElement element) -> action.moveToElement(element).perform();
        wait.until(ExpectedConditions.elementToBeClickable(catalogLink));
        hover.accept(catalogLink);
        wait.until(ExpectedConditions.elementToBeClickable(productsLink));
        productsLink.click();
    }
}