package com.qatestlab.task4.pages;

import com.qatestlab.task4.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Optional;

import static com.qatestlab.task4.utils.Properties.getBaseAdminUrl;

public class LoginPage extends BasePage {

    @FindBy(id = "email")
    private WebElement username;

    @FindBy(id = "passwd")
    private WebElement password;

    @FindBy(css = "button[type='submit']")
    private WebElement loginBtn;

    private LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public static LoginPage init(WebDriver driver) {
        return new LoginPage(driver);
    }

    public LoginPage launch() {
        driver.get(getBaseAdminUrl());
        return this;
    }

    public LoginPage setUsername(String username) {
        this.username.sendKeys(username);
        return this;
    }

    public LoginPage setPassword(String password) {
        this.password.sendKeys(password);
        return this;
    }

    public void login() {
        this.loginBtn.click();
    }

    @Override
    public boolean isAt() {
        Optional<Boolean> result = Optional.ofNullable(username.isDisplayed());
        return result != null ? true : false;
    }
}